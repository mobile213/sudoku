import 'package:sudoku_project/sudoku_project.dart' as sudoku_project;
import 'dart:io';
// import 'dart:developer' as developer;

void main(List<String> arguments) {
  List<List<int>> level = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
  ];

  List<List<int>> levelAnswer = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
  ];

  List<List<int>> grid = [
    [9, 0, 4, 5, 0, 1, 2, 7, 0],
    [0, 5, 0, 0, 4, 0, 1, 0, 8],
    [6, 0, 3, 8, 2, 7, 9, 0, 0],
    [8, 3, 0, 4, 0, 0, 7, 5, 0],
    [0, 0, 5, 0, 0, 8, 0, 0, 3],
    [1, 0, 6, 7, 5, 0, 4, 8, 0],
    [4, 0, 9, 0, 8, 0, 0, 6, 7],
    [5, 6, 1, 0, 7, 0, 8, 2, 0],
    [0, 0, 8, 9, 0, 2, 0, 1, 0],
  ];

  List<List<int>> grid2 = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9],
  ];

  List<List<int>> answerGrid = [
    [9, 8, 4, 5, 3, 1, 2, 7, 6],
    [2, 5, 7, 6, 4, 9, 1, 3, 8],
    [6, 1, 3, 8, 2, 7, 9, 4, 5],
    [8, 3, 2, 4, 9, 6, 7, 5, 1],
    [7, 4, 5, 2, 1, 8, 6, 9, 3],
    [1, 9, 6, 7, 5, 3, 4, 8, 2],
    [4, 2, 9, 1, 8, 5, 3, 6, 7],
    [5, 6, 1, 3, 7, 4, 8, 2, 9],
    [3, 7, 8, 9, 6, 2, 5, 1, 4],
  ];

  List<List<int>> answerGrid2 = [
    [5, 3, 4, 6, 7, 8, 9, 1, 2],
    [6, 7, 2, 1, 9, 5, 3, 4, 8],
    [1, 9, 8, 3, 4, 2, 5, 6, 7],
    [8, 5, 9, 7, 6, 1, 4, 2, 3],
    [4, 2, 6, 8, 5, 3, 7, 9, 1],
    [7, 1, 3, 9, 2, 4, 8, 5, 6],
    [9, 6, 1, 5, 3, 7, 2, 8, 4],
    [2, 8, 7, 4, 1, 9, 6, 3, 5],
    [3, 4, 5, 2, 8, 6, 1, 7, 9],
  ];

  void printAnswer(List<List<int>> levelAnswer) {
    for (int i = 0; i < 9; (++i)) {
      if ((i % 3) == 0) {
        print("\x1B[31m --------------------------------- \x1B[0m");
      }
      for (int j = 0; j < 9; (++j)) {
        if ((j % 3) == 0) {
          stdout.write("\x1B[31m| \x1B[0m");
        }
        if (levelAnswer[i][j] == 0) {
          stdout.write("   ");
        } else {
          stdout.write(" ");
          stdout.write(levelAnswer[i][j]);
          stdout.write(" ");
        }
      }
      print("\x1B[31m |\x1B[0m");
    }
    print("\x1B[31m --------------------------------- \x1B[0m");
  }

  void printMySudoku(List<List<int>> level) {
    print("------🧩🎮🕹 SuDoKu 🕹🎮🧩--------");
    print("\x1B[35m   0  1  2    3  4  5    6  7  8   \x1B[0m");
    for (int i = 0; i < 9; (++i)) {
      if ((i % 3) == 0) {
        print("\x1B[31m --------------------------------- \x1B[0m");
      }
      for (int j = 0; j < 9; (++j)) {
        if ((j % 3) == 0) {
          stdout.write("\x1B[31m| \x1B[0m");
        }
        if (level[i][j] == 0) {
          stdout.write("   ");
        } else {
          stdout.write(" ");
          stdout.write(level[i][j]);
          stdout.write(" ");
        }
      }
      print("\x1B[31m | \x1B[0m" + "\x1B[35m  $i \x1B[0m");
    }
    print("\x1B[31m --------------------------------- \x1B[0m");
  }

  bool removeVal(int row, int col, int myVal) {
    level[row][col] = 0;
    return true;
  }

  bool checkRow(int row, int col, int myVal) {
    for (int a = 0; a < 9; (++a)) {
      if (myVal == level[row][a]) {
        print('🚫 $myVal Already in Row:  $row');
        return false;
      }
    }
    return true;
  }

  bool checkCol(int row, int col, int myVal) {
    for (int b = 0; b < 9; (++b)) {
      if (myVal == level[b][col]) {
        print('🚫 $myVal Already in Column: $col');
        return false;
      }
    }
    return true;
  }

  bool checkBox(int row, int col, int myVal) {
    int boxRowOffset = ((row ~/ 3) * 3);
    int boxColOffset = ((col ~/ 3) * 3);
    for (int c = 0; c < 3; (++c)) {
      for (int d = 0; d < 3; (++d)) {
        if (myVal == level[boxRowOffset + c][boxColOffset + d]) {
          print('🚫 $myVal Already in Box ');
          return false;
        }
      }
    }
    return true;
  }

  bool insertVal(int row, int col, int myVal) {
    print(
        '\x1B[33m Entered insertVal row $row column $col myVal $myVal \x1B[0m');
    if (checkRow(row, col, myVal) == false) {
      return false;
    }
    if (checkCol(row, col, myVal) == false) {
      return false;
    }
    if (checkBox(row, col, myVal) == false) {
      return false;
    }
    level[row][col] = myVal;
    return true;
  }

  int row;
  int col;
  int val;
  String action;
  int levelSelect;

  print("------------------------------------");
  print("(☞ﾟヮﾟ)☞ 🧩🎮🕹 SuDoKu 🕹🎮🧩 ☜(ﾟヮﾟ☜)");
  print("------------------------------------");
  print("Please Select the level of SUDOKU ⌛⌛ ");
  print("Level Select 1 or 2 ❔❓❔ ");
  levelSelect = int.parse(stdin.readLineSync()!);
  if (levelSelect == 1) {
    level = grid;
    levelAnswer = answerGrid;
  }
  if (levelSelect == 2) {
    level = grid2;
    levelAnswer = answerGrid2;
  }
  printMySudoku(level);

  while (true) {
    print("Enter I for insert or R for remove and X to Answer : ");
    action = (stdin.readLineSync()!);
    if (action == "I" || action == "R") {
      print("Row: (0-8) ");
      row = int.parse(stdin.readLineSync()!);
      print("Column: (0-8) ");
      col = int.parse(stdin.readLineSync()!);
      print("Value: ");
      val = int.parse(stdin.readLineSync()!);
      if (action == "I") {
        if (insertVal(row, col, val)) {
          print("Good Job 👍😁 ");
        } else {
          print("Try Again ❗❗ ");
        }
        printMySudoku(level);
      }
      if (action == "R") {
        removeVal(row, col, val);
        printMySudoku(level);
      }
    }
    if (action == "X") {
      print("------------ANSWER-------------");
      printAnswer(levelAnswer);
      break;
    }
  }
}
